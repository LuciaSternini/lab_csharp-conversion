# Trasposizione da Java a C#

In questa esercitazione verrà mostrato come esempio il processo di trasposizione di un progetto non banale scritto in Java in un equivalente progetto C#.

È improprio parlare di mera traduzione in quanto non si tratta di una semplice riscrittura del codice, bensì di un processo più ampio che tiene conto anche delle convenzioni, delle prassi e della libreria standard comunemente usati nel linguaggio obiettivo, cioè C#. 

Il progetto da tradurre implementa il gioco MNK, cioè il "tris" (o TicTacToe) generalizzato, in cui due giocatori umani competono a turno con lo scopo di allineare orizzontalmente, verticalmente, o diagonalmente `K` simboli (croci o cerchi) su una griglia con `M` righe e `N` colonne, inserendo un simbolo a testa per turno.

## Preparazione ambiente di lavoro

0. Effettuare un fork del presente repository e clonare il proprio fork sulla macchina locale 
    
    * Al termine del processo dovrebbe essere presente sul file system locale una cartella chiamata `courses-2018-oop-lab-java-csharp-conversion`
    
    * Essa contiene una sotto-cartella, `java`, contenente il codice da tradurre, ed un'altra, `csharp`, contenente una sua traduzione parziale

0. Avviare Eclipse __scegliendo come workspace__ la cartella `courses-2018-oop-lab-java-csharp-conversion/java`
    
    * Si noti che il workspace contiene due progetti tra loro interdipendenti:
        
        - `mnk-lib` libreria di supporto per il gioco MNK, indipendente dallo specifico rendering scelto
        
        - `mnk-console` progetto eseguibile che sfrutta il progetto `mnk-lib` e un'interfaccia a riga di comando per permettere a 2 giocatori umani di sfidarsi al gioco MNK
    
    * Si esegua la procedura per importare dei progetti esistenti nel workspace:
    
        - si scelga come cartella in cui cercare i progetti la cartella `courses-2018-oop-lab-java-csharp-conversion/java`, ovvero il workspace stesso
    
        - lo wizard di importazione dovrebbe a questo punto trovare da solo i due progetti di cui sopra
    
        - finalizzare la procedura d'importazione
    
    * È possibile mettere in esecuzione il gioco lanciando la classe principale `it.unibo.oop.mnk.ui.console.Program` presente nel progetto `mnk-console`, ed interagire poi con esso mediante il terminale 
    
    * È consigliabile provare ad eseguire il gioco almeno una volta per verificare che il workspace sia correttamente configurato e che tutto compili
    
    * In caso di errori/problemi in questa fase, segnalare tempestivamente al docente la situazione 

0. Avviare Visual Studio (o MonoDevelop, o Rider) __aprendo poi la soluzione__ `courses-2018-oop-lab-java-csharp-conversion-/csharp/MNK/MNK.sln`
    
    * Si noti che la soluzione contiene due progetti tra loro interdipendenti:
        
        - `Exercise1` semplice esercizio di traduzione autocontenuto. Da svolgere oggi come "riscaldamento"
        
        - `Exercise2` altro esercizio di traduzione autocontenuto. Da svolgere oggi come "riscaldamento"
        
        - `MNKGameLib` porting C# di `mnk-lib`
        
        - `MNKConsole` porting C# di `mnk-console`
    
    * Al caricamento della soluzione, non dovrebbero sussistere errori di compilazione
    
    * In caso di errori/problemi in questa fase, segnalare tempestivamente al docente la situazione 

## Esercizio 1

Questo esercizio di riscaldamento richiede che si traduca la classe Java `it.unibo.oop.util.TupleImpl`, presente nel progetto `mnk-lib`, nel suo corrispettivo C#, il cui scheletro è già stato predisposto nella classe .NET `Unibo.Oop.Utils.TupleImpl`, presente nel progetto `Exercise1`.

Affinché l'esercizio sia considerato valido, è necessario the il programma di test `Unibo.Oop.Utils.Program`, presente nel progetto `Exercise1`, arrivi a stampare l'output `OK`.

Si noti che, in generale, la class library standard di .NET espone già di per sé delle classi per rappresentare il concetto di __tupla__.
Negli esercizi successivi, e in generale sviluppando in ambiente .NET, qualora di renda necessario, sarà opportuno utilizzare la nozione di tupla fornita dalla libreria standard.

## Esercizio 2

Questo esercizio di riscaldamento richiede che si traduca il package Java `it.unibo.oop.events.*`, presente nel progetto `mnk-lib`, nel suo corrispettivo C#.
Alcuni elementi (ad esempio le interfacce) sono già stati tradotti e sono disponibili nel namespace `Unibo.Oop.Events`, presente nel progetto `Exercise2`.

Affinché l'esercizio sia considerato valido, è necessario the il programma di test `Unibo.Oop.Events.Program`, presente nel progetto `Exercise2`, arrivi a stampare l'output `OK`.

Si noti che, in generale, .NET espone già nei suoi linguaggi la nozione di __sorgente ed ascoltatore di eventi__, a livello di costrutto di prima classe.
Negli esercizi successivi, e in generale sviluppando in ambiente .NET, qualora di renda necessario, sarà opportuno utilizzare la nozione di evento fornita da .NET.

## Esercizio 3

Questo esercizio richiede che si traducano nella loro interezza i progetti `mnk-lib` e `mnk-console` nei corrispettivi C# `MNKGameLib` e `MNKConsole`.

Com'è facile notare guardando il codice C#, parte della trasposizione è già stata svolta, a meno dell'implementazione di alcuni metodi o classi, spesso sostituiti dal lancio di una `NotImplementedException`.

Scopo finale dell'esercizio è implementare le parti mancanti fino ad ottenere una resa del gioco MNK uguale a quella della sua versione Java.

*Trattandosi di un progetto non banale, si consiglia di attendere che il docente introduca l'esercizio corrente, fornendo in particolare una visione architetturale ed una chiave di lettura per il codice Java fornito, oltre che dei consigli di alto livello per impostare la traduzione.*

Nello svolgere la trasposizione si ponga particolare attenzione a rispettare in ogni linguaggio le convenzioni opportune, sia sintattiche che stilistiche che architetturali, ad esempio, in ordine sparso:

* .NET usa le proprietà, Java usa getter e setter

* le interfacce C# vogliono il prefisso `I`, le intefacce Java no

* il numero di elementi di una collezione C# di ottiene con la proprietà d'istanza `Count`, in Java si usa il metodo `size()`

* .NET supporta nativamente eventi e tuple, in Java questi concetti vanno ricostruiti

* in .NET si usano i `Nullable` per i tipi primitivi, in Java si usano gli `Optional` per i tipi non primitivi

* .NET usa gli `IEnumerable` laddove Java userebbe degli `Stream`

* gli `enum` in Java sono oggetti, quindi "tipi riferimento", mentre in .NET sono strutture, quindi "tipi valore"

    - quindi non si può usare `null` in .NET lavorando con gli `enum`...

* le interfacce in .NET _non_ accettano membri statici, le interfacce Java sì

* eccetera

Per eventuali dubbi, consultare il docente, le slide, oppure la [documentazione ufficiale](https://docs.microsoft.com/en-us/dotnet/api/?view=netframework-4.7.2) (si consiglia di consultarla in inglese: la versione italiana è tradotta automaticamente e poco comprensibile).

## Esercizio 4

Si completino gli esercizi su C# forniti durante il [laboratorio precedente](https://bitbucket.org/angelocroatti/courses-2018-oop-lab-csharp).

