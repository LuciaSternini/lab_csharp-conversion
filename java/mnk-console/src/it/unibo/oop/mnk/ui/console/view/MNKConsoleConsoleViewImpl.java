package it.unibo.oop.mnk.ui.console.view;

import it.unibo.oop.events.EventListener;
import it.unibo.oop.mnk.MNKMatch;
import it.unibo.oop.mnk.MatchEventArgs;
import it.unibo.oop.mnk.Symbols;
import it.unibo.oop.mnk.TurnEventArgs;
import it.unibo.oop.mnk.ui.console.view.MNKConsoleView;
import it.unibo.oop.util.ImmutableMatrix;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class MNKConsoleConsoleViewImpl implements MNKConsoleView {

    private MNKMatch match = null;

    @Override
    public void renderEnd(MNKMatch model, int turn, Optional<Symbols> winner, ImmutableMatrix<Symbols> state) {
        System.out.print("GAME OVER!");
        if (winner.isPresent()) {
            System.out.printf(" %s wins!\n", winner.get());
        } else {
            System.out.println();
        }
    }

    @Override
    public void renderNextTurn(MNKMatch m, int turn, Symbols player, ImmutableMatrix<Symbols> grid) {
        System.out.printf("MNK-Game %dx%dx%d -- Turn %d -- Player: %s\n", grid.getRowsSize(), grid.getColumnsSize(), m.getK(), turn, player.name());

        System.out.print("\t\t  ");
        System.out.println(
                IntStream.rangeClosed(1, grid.getColumnsSize())
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(" "))
        );
        
        for (int i = 0; i < grid.getRowsSize(); i++) {
        	System.out.print("\t\t");
            System.out.print(int2Char(i));
            System.out.print(" ");
            System.out.println(renderRow(grid, i));
        }

        System.out.printf("\tWhere do you want to put a %s? (pattern: <row> <column>)\n\t> ", player);

    }
    
    private char int2Char(int x) {
        return (char)('a' + x);
    }
    
    private String renderRow(ImmutableMatrix<Symbols> grid, int i) {
    	return grid.getRow(i).map(Object::toString).collect(Collectors.joining(" "));
    }

    @Override
    public MNKMatch getModel() {
        return match;
    }

    @Override
    public void setModel(MNKMatch model) {
    	if (match != null) {
    		 this.match.turnEnded().unbind(onTurnEnded);
    		 this.match.matchEnded().unbind(onMatchEnded);
    		 this.match.turnBeginning().unbind(onTurnBeginning);
    		 this.match.errorOccurred().unbind(onErrorOccurred);
    		 this.match.resetPerformed().unbind(onResetPerformed);
    	}
        this.match = model;
        if (match != null) {
        	this.match.turnEnded().bind(onTurnEnded);
        	this.match.matchEnded().bind(onMatchEnded);
        	this.match.turnBeginning().bind(onTurnBeginning);
        	this.match.errorOccurred().bind(onErrorOccurred);
        	this.match.resetPerformed().bind(onResetPerformed);
        }
    }
    
    private final EventListener<MatchEventArgs> onMatchEnded = data -> {
        renderNextTurn(data.getSource(), data.getTurn(), data.getSource().getCurrentPlayer(), data.getSource().getGrid());
        renderEnd(data.getSource(), data.getTurn(), data.getWinner(), data.getSource().getGrid());
    };
    
    private final EventListener<TurnEventArgs> onTurnEnded = data -> {
        System.out.println("\tGreat move!");
    };
    
    private final EventListener<TurnEventArgs> onTurnBeginning = data -> {
        renderNextTurn(data.getSource(), data.getTurn(), data.getPlayer(), data.getSource().getGrid());
    };
    
    private final EventListener<Exception> onErrorOccurred = data -> {
        System.err.println(data.getMessage());
    };
    
    private final EventListener<MNKMatch> onResetPerformed = data -> {
        renderNextTurn(data, data.getTurn(), data.getCurrentPlayer(), data.getGrid());
    };
}
